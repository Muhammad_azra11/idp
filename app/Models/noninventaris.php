<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class noninventaris extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_barang',
        'stock',
        'keterangan_barang',
        'kuantitas',
        'keadaan_barang',
        'keterangan'
    ];
}
