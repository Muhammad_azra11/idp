<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class banggunan extends Model
{
    use HasFactory;

    protected $table = 'banggunans';
    protected $primarikey = 'id';
    protected $fillable = [
        // 'id',
        'kode_lokasi',
        'kode_barang',
        'nomor_register',
        'nama_barang',
        'kondisi_banggunan',
        'luas_lantai',
        'letak_lokasi',
        'tgl_dokumen',
        'tgl_digunakan',
        'nomor_dokumen',
        'kode_tanah',
        'luas',
        'status',
        'asal_usul',
        'harga',
        'keterangan'
    ];
}
