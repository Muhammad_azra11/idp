<?php

namespace App\Http\Controllers;
use App\Models\Tanah;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Session as SessionSession;

class InventarisController extends Controller
{
    public function index() {

        $tanah = Tanah::latest()->paginate(5);

        return view('admin.inventaris.tanah.tanah', compact('tanah'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'kode_tanah' => 'required',
        ]);

        $tanah = Tanah::create($request->all());
        
        if($tanah){
            Session()->flash('status', 'success');
            Session()->flash('massage', 'berhasil menambahkan');
        }

        return redirect()->route('tanah.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    public function edit($id){
        $tanah = Tanah::findOrFail($id);
        return view('admin.inventaris.tanah.tanahedit', compact('tanah'));
    }

    public function update(Request $request, $id) {
        $tanah = Tanah::findOrFail($id);
        $tanah->update($request->all());

        return redirect()->route('tanah.index');
    }
}
