<?php

namespace App\Http\Controllers;

use Tharowable;
use App\Models\banggunan;
use App\Models\Peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeminjamController extends Controller
{
    public function index() {
        return view('peminjam.dashboard');
     }

     public function banggunan() {
      $banggunan = banggunan::all();
        return view('peminjam.Inventaris.index', compact('banggunan'));
     }

     public function store(Request $request)
    {

      $peminjaman = banggunan::findOrFail($request->id_banggunan)->only('status');

      if($peminjaman['status'] != 'kosong') {
         Session()->flash('status', 'Ruangan sedang dipakai !!');
         Session()->flash('alert-class', 'alert-danger');

         return redirect('banggunan');
      }
      else{
          try{
            // DB::beinTransaction();
            Peminjaman::create($request->all());
            // banggunan::where('status')->update(['status' => 'dipakai']);
            $ganti = banggunan::findOrFail($request->id_banggunan);
            $ganti->status = 'dipakai';
            $ganti->save();
            DB::commit();

            Session()->flash('status', 'Berhasil Meminjam');
            Session()->flash('alert-class', 'alert-success');
            return redirect('banggunan');

          }catch(\Tharowable $th) {
            DB::rollBack();
          }
      }
     }
}
