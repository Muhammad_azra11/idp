<?php

namespace App\Http\Controllers;

use App\Models\banggunan;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class BanggunanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banggunan = banggunan::all();

        return view('admin.inventaris.banggunan.banggunan', compact('banggunan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banggunan = banggunan::create([
            'status' => 'kosong',
            $request->all()
        ]);

        if($banggunan){
            Session()->flash('status', 'success');
            Session()->flash('massage', 'berhasil Ditambahkn !!');
        }

        return redirect()->route('banggunan.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $banggunan = banggunan::all();
        // $anjing = banggunan::find($id);
        //  dd($anjing);
        $banggunan = banggunan::findOrFail($id);

        return view('admin.inventaris.banggunan.banggunanedit', compact('banggunan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $banggunan = banggunan::where('id', $request->id)
        // ->update([
        //     $request->all()
        // ]);
        // return redirect()->route('ubahbanggunan.edit');
       
        // banggunan::where('id', $request->$id)->update([
        //     $request->all()
        // ]);

        $banggunan = banggunan::findOrFail($id);

        $banggunan->update($request->all());
        return redirect()->route('banggunan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
