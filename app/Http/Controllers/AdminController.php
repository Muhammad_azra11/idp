<?php

namespace App\Http\Controllers;

use App\Models\banggunan;
use App\Models\Tanah;
use Illuminate\Http\Request;

class AdminController extends Controller
{
     public function index() {

      $tanah = Tanah::count();
      $banggunan = banggunan::count();
        return view('admin.dashboard', compact('tanah', 'banggunan'));
     }
}
