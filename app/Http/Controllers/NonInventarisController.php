<?php

namespace App\Http\Controllers;

use App\Models\NonInventaris;
use App\Models\noninventaris as ModelsNoninventaris;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use PDO;

class NonInventarisController extends Controller
{
    public function index()
    {
        $non = ModelsNoninventaris::all();
        return view('admin.noninventaris.index', compact('non'));
    }

    public function store (Request $request) {
        $non = ModelsNoninventaris::create($request->all());
        if($non){
            Session()->flash('status', 'success');
            Session()->flash('massage', 'berhasil menambahkan');
        }

        return redirect()->route('non.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    public function edit($id) {
        $edit = ModelsNoninventaris::findOrFile($id);
        return view('admin.noninventaris.indexedit', compact('edit'));
    }
}
