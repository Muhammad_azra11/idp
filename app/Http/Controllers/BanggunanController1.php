<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\banggunan;

class BanggunanController2 extends Controller
{
    public function index() {

        $banggunan = banggunan::all();

        return view('admin.inventaris.banggunan.banggunan', compact('banggunan'));
    }

    public function store(Request $request) {
        banggunan::create($request->all());

        return redirect()->route('banggunan.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    public function edit(banggunan $banggunan) {
         return view('admin.inventaris.banggunan.banggunanedit', compact('banggunan'));
    }
}
