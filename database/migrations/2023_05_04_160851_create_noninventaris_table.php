<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noninventaris', function (Blueprint $table) {
            $table->id();
            $table->string('nama_barang');
            $table->string('stock');
            $table->string('keterangan_barang');
            $table->string('kuantitas');
            $table->string('keadaan_barang');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noninventaris');
    }
};
