<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banggunans', function (Blueprint $table) {
            $table->id();
            $table->string('kode_lokasi');
            $table->string('kode_barang');
            $table->string('nomor_register');
            $table->string('nama_barang');
            $table->string('kondisi_banggunan');
            $table->string('luas_lantai');
            $table->string('letak_lokasi');
            $table->string('tgl_dokumen');
            $table->string('tgl_digunakan');
            $table->string('nomor_dokumen');
            $table->string('kode_tanah');
            $table->string('luas');
            $table->enum('status', ['kosong', 'dipakai']);
            $table->string('asal_usul');
            $table->string('harga');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banggunans');
    }
};
