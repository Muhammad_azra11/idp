<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanahs', function (Blueprint $table) {
            $table->id();
            $table->integer('kode_tanah');
            $table->integer('no_sertifikat');
            $table->string('nama_pemilik');
            $table->string('status');
            // $table->string('status');
            $table->integer('luas');
            $table->string('sumber_perolehan');
            $table->date('tgl_perolehan');
            $table->string('letak_tanah');
            $table->string('penggunaan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanahs');
    }
};
