<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>My Example</title>

<!-- CSS -->
<style>
    .judul {
    font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
    font-size: 20px;
    width: 20em;
    padding: 1em;
    /* border: 1px solid #ccc; */
    margin-bottom: 3px;
    padding-left: 500px;
    }

.myForm {
font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
font-size: 0.8em;
width: 20em;
padding: 1em;
/* border: 1px solid #ccc; */
margin-bottom: 30px;
padding-left: 500px;
}

.myForm * {
box-sizing: border-box;
}

.myForm fieldset {
border: none;
padding: 0;
}

.myForm legend,
.myForm label {
padding: 0;
font-weight: bold;
}

.myForm label.choice {
font-size: 0.9em;
font-weight: normal;
}

.myForm input[type="text"],
.myForm input[type="tel"],
.myForm input[type="email"],
.myForm input[type="datetime-local"],
.myForm input[type="number"],
.myForm select,
.myForm textarea {
display: block;
width: 100%;
border: 1px solid #ccc;
font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
font-size: 0.9em;
padding: 0.3em;
}

.myForm textarea {
height: 100px;
}

.btnn{
  border: 1px solid black;
  padding: 5px;
  border-radius: 5px;
  text-decoration: none;
}

.myForm button {
padding: 1em;
border-radius: 0.5em;
background: #eee;
border: none;
font-weight: bold;
margin-top: 1em;
}

.myForm button:hover {
background: #ccc;
cursor: pointer;
}

table {
  font-family: arial, sans-serif;
  padding-top: 50px;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

h2{
    font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
    font-size: 20px;
    width: 20em;
    padding: 1em;
    /* border: 1px solid #ccc; */
    margin-bottom: 3px;
    padding-left: 500px;
}

.full {
  position: relative;
}

.full-content{
  display: none;
  position: absolute;
  z-index: 1;
}

.full-content > a {
  display: block;
  border: 1px solid black;
  padding: 4px;
  text-decoration: none;
  margin-top: 2px;
  color: black;
  background: #0d6ad7; 
  border-radius: 5px;
}

.full:hover .full-content{
  display: flex;
}

.fullbtn {
 background: none;
 border: 1px solid black;
 padding: 3px;
 box-shadow: 0 0 2px 2px #fff;
}
</style>

</head>
<body>
  <h1 class="judul">Inventaris Tanah</h1>
<form class="myForm" method="post"  action="/tanah/{{$tanah->id}}" enctype="multipart/form-data">
    @method('put')
    @csrf
<p>
<label>Kode Tanah
<input type="number" name="kode_tanah" required>
</label> 
</p>

<p>
<label>No Sertifikat 
<input type="number" name="no_sertifikat">
</label>
</p>

<p>
<label>Nama Pemilik 
<input type="text" name="nama_pemilik">
</label>
</p>

<p>
    <label>Status
    <select id="pickup_place" name="status">
    <option value="" selected="selected">Select One</option>
    <option value="office" >Taxi Office</option>
    <option value="town_hall" >Town Hall</option>
    <option value="telepathy" >We'll Guess!</option>
    </select>
    </label> 
</p>
    
<p>
    <label>Luas (M) 
    <input type="number" name="luas">
    </label>
    </p>

    <p>
        <label>Sumber Perolehan
        <select id="pickup_place" name="sumber_perolehan">
        <option value="" selected="selected">Select One</option>
        <option value="office" >Taxi Office</option>
        <option value="town_hall" >Town Hall</option>
        <option value="telepathy" >We'll Guess!</option>
        </select>
        </label> 
    </p>

    <p>
        <label>Tanggal Perolehan
        <input type="datetime-local" name="tgl_perolehan" required>
        </label>
        </p>

        <p>
            <label>Letak Tanah
            <input type="text" name="letak_tanah">
            </label>
            </p>
        <p>
            <label>Penggunaan
            <input type="text" name="penggunaan">
            </label>
            </p>
            
            <p><button type="submit">Submit Booking</button></p>
            <a class="btnn" href="{{url('administrator')}}">asas</a>
{{-- 
<fieldset>
<legend>Which taxi do you require?</legend>
<p><label class="choice"> <input type="radio" name="taxi" required value="car"> Car </label></p>
<p><label class="choice"> <input type="radio" name="taxi" required value="van"> Van </label></p>
<p><label class="choice"> <input type="radio" name="taxi" required value="tuktuk"> Tuk Tuk </label></p>
</fieldset>

<fieldset>
<legend>Extras</legend>
<p><label class="choice"> <input type="checkbox" name="extras" value="baby"> Baby Seat </label></p>
<p><label class="choice"> <input type="checkbox" name="extras" value="wheelchair"> Wheelchair Access </label></p>
<p><label class="choice"> <input type="checkbox" name="extras" value="tip"> Stock Tip </label></p>
</fieldset>

<p>
<label>Pickup Date/Time
<input type="datetime-local" name="pickup_time" required>
</label>
</p>
	
<p>
<label>Pickup Place
<select id="pickup_place" name="pickup_place">
<option value="" selected="selected">Select One</option>
<option value="office" >Taxi Office</option>
<option value="town_hall" >Town Hall</option>
<option value="telepathy" >We'll Guess!</option>
</select>
</label> 
</p>

<p>
<label>Dropoff Place
<input type="text" name="dropoff_place" required list="destinations">
</label>

<datalist id="destinations">
<option value="Airport">
<option value="Beach">
<option value="Fred Flinstone's House">
</datalist>
</p>

<p>
<label>Special Instructions
<textarea name="comments" maxlength="500"></textarea>
</label>
</p>

<p><button>Submit Booking</button></p>
 --}}
</form>

{{-- <h2>Data Invantoris</h2>
<table>
  <thead>
  <tr>
    <th>Kode Tanah</th>
    <th>Nomor Sertifikat</th>
      <th>Nama Pemilik</th>
      <th>Letak Tanah</th>
      <th>Luas</th>
      <th>Tanggal Perolehan</th>
      <th>Status</th>
      <th>Option</th>
    </tr>
  </thead> --}}
  {{-- <tbody> --}}
    {{-- @foreach($tanah as $tana)
    <tr>
      
      <td>{{$tana->kode_tanah}}</td>
      <td>{{$tana->no_sertifikat}}</td>
      <td>{{$tana->nama_pemilik}}</td>
      <td>{{$tana->letak_tanah}}</td>
      <td>{{$tana->luas}}</td>
      <td>{{$tana->tgl_perolehan}}</td>
      <td>{{$tana->status}}</td>
      <td>
        <div  class="full">
        <button class="fullbtn">
          Option
        </button>
        <div class="full-content">
          <a href="">Edit</a>
          <a href="">Hapus</a>
        </td>
    </div>
      </div>
    </tr> --}}
  {{-- </tbody> --}}
    {{-- <tr>
      <td>nn</td>
      <td>Francisco Chang</td>
      <td>Mexico</td>
    </tr>
    <tr>
      <td>Ernst Handel</td>
      <td>Roland Mendel</td>
      <td>Austria</td>
    </tr>
    <tr>
      <td>Island Trading</td>
      <td>Helen Bennett</td>
      <td>UK</td>
    </tr>
    <tr>
      <td>LWinecellars</td>
      <td>Yoshi Tannamuri</td>
      <td>Canada</td>
    </tr>
    <tr>
      <td>Riuniti</td>
      <td>Giovanni Rovelli</td>
      <td>Italy</td>
    </tr> --}}
    {{-- @endforeach --}}
  {{-- </table> --}}

</body>
</html>