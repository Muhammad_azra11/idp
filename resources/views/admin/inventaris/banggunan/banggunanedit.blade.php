<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  
  <!-- Design by foolishdeveloper.com -->
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <style media="screen">
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap');
    html,
body {
	height: 100%;
}

body {
	margin: 0;
	background: linear-gradient(45deg, #49a09d, #5f2c82);
	font-family: sans-serif;
	font-weight: 100;
}

.data {
    
    padding-top: 110px;
    padding-bottom: 50px;
    padding-left: 280px
}


.container {
	/* position: absolute; */
	padding-top: 110px;
    padding-left: 280px
	/* left: 50%; */
	/* transform: translate(-50%, -50%); */
}

table {
	width: 800px;
	border-collapse: collapse;
	overflow: hidden;
	box-shadow: 0 0 20px rgba(89, 81, 81, 0.1);
}

th,
td {
	padding: 15px;
	background-color: rgba(255,255,255,0.2);
	color: #fff;
}

th {
	text-align: left;
}

thead {
	th {
		background-color: #55608f;
	}
}

tbody {
	tr {
		&:hover {
			background-color: rgba(255,255,255,0.3);
		}
	}
	td {
		position: relative;
		&:hover {
			&:before {
				content: "";
				position: absolute;
				left: 0;
				right: 0;
				top: -9999px;
				bottom: -9999px;
				background-color: rgba(255,255,255,0.2);
				z-index: -1;
			}
		}
	}
}

*{
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  list-style: none;
  font-family: 'Montserrat', sans-serif;
}

body{
  background: linear-gradient(
     105deg,
     #88beee ,
     #0a2e73
 );
}

.wrapper{
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
}

.registration_form{
  background: white;
  padding: 25px;
  border-radius: 5px;
  width: 400px;
}

.registration_form .title{
  text-align: center;
  font-size: 25px;
  text-transform: uppercase;
  color: white;
  background:rgb(13, 98, 215);
  letter-spacing: 2px;
  font-weight: 700;
  margin-top: -25px;
  margin-left:-25px;
  margin-right:-25px;
}

.form_wrap{
  margin-top: 35px;
}

.form_wrap .input_wrap{
  margin-bottom: 15px;
}

.form_wrap .input_wrap:last-child{
  margin-bottom: 0;
}

.form_wrap .input_wrap label{
  display: block;
  margin-bottom: 3px;
  color: #1a1a1f;
}

.form_wrap .input_grp{
  display: flex;
  justify-content: space-between;
}

.form_wrap .input_grp  input[type="text"]{
  width: 165px;
}
.form_wrap .input_grp  input[type="number"]{
  width: 165px;
}

.form_wrap  input[type="text"]{
  width: 100%;
  border-radius: 3px;
  border: 1.3px solid #9597a6;
  padding: 10px;
  outline: none;
}

.form_wrap  input[type="text"]:focus{
  border-color: #063abd;
}

.form_wrap ul{
 border:1px solid rgb(115, 185, 235);
  width:70%;
  background: rgb(206, 238, 242);
  margin-left: 15%;
  padding: 8px 10px;
  border-radius: 20px;
  display: flex;
  justify-content: center;
}

.form_wrap ul li:first-child{
  margin-right: 15px;
}

.form_wrap ul .radio_wrap{
  position: relative;
  margin-bottom: 0;
}

.form_wrap ul .radio_wrap .input_radio{
  position: absolute;
  top: 0;
  right: 0;
  opacity: 0;
}

.form_wrap ul .radio_wrap span{
  display: inline-block;
  font-size: 17px;
  padding: 3px 15px;
  border-radius: 15px;
  color: #101749;
}

.form_wrap .input_radio:checked ~ span{
  background: #105ce2;
  color:white;
}

.form_wrap .submit_btn{
  width: 100%;
  background: #0d6ad7;
  padding: 10px;
  border: 0;
  color:white;
  font-size:17px;
  border-radius: 3px;
  text-transform: uppercase;
  letter-spacing: 2px;
  cursor: pointer;
}

.form_wrap .submit_btn:hover{
  background: #051c94;
}
.btnn {
  text-decoration: none;
  padding: 15px;
  border: 1px solid black;
  background: #9597a6;
  padding-top: 2px;
}
.dropdown{
  position: relative;
}
.dropdown-content{
  display: none;
  position: absolute;
  z-index: 1;
}
.dropdown-content > a {
  display: block;
  border: 1px solid black;
  padding: 4px;
  text-decoration: none;
  margin-top: 2px;
  color: white;
  background: #0d6ad7; 
  border-radius: 5px;
}
.dropdown:hover .dropdown-content{
  display: block;
}
.dropobtn {
 background: none;
 border: 1px solid black;
 padding: 3px;
 box-shadow: 0 0 2px 2px #fff;
}
  </style>
</head>
<body>
  <div class="wrapper">
    <div class="registration_form">
      <div class="title">
        Inventaris Banggunan
      </div>
      {{-- {{$banggunan}} --}}
          {{-- @foreach($banggunan as $ba) --}}
      <form action="/banggunan/{{$banggunan->id}}" method="post">
        @method('put')
        @csrf
        <div class="form_wrap">
          {{-- <div class="input_grp"> --}}
            <div class="input_wrap">
              <label for="fname">Kode Lokasi</label>
              <input type="text" value="" name="kode_lokasi" value="{{$banggunan->name}}">
            </div>
            <div class="input_wrap">
              <label for="lname">Kode Barang</label>
              <input type="text" name="kode_barang">
            </div>
          {{-- </div> --}}
          <div class="input_wrap">
            <label for="email">Nomor Registrasi</label>
            <input type="text" name="nomor_register">
          </div>
          <div class="input_wrap">
            <label for="email">Nama Barang</label>
            <input type="text" name="nama_barang">
          </div>

          <div class="input_wrap">
            <label for="city">Kondisi Barang</label>
            <input type="text" name="kondisi_banggunan">
          </div>
          <div class="input_wrap">
            <label for="country">Luas Lantai</label>
            <input type="text" name="luas_lantai" >
          </div>
          <div class="input_wrap">
            <label for="country">Letak Lokasi</label>
            <input type="text"  name="letak_lokasi">
          </div>
          <div class="input_wrap">
            <label for="country">Tanggal Dokument</label>
            <input type="text" name="tgl_dokumen" >
          </div>
          <div class="input_wrap">
            <label for="country">Tanggal Digunakan</label>
            <input type="text" name="tgl_digunakan" >
          </div>
          <div class="input_wrap">
            <label for="country">Kode Tanah</label>
            <input type="text" name="kode_tanah">
          </div>
          <div class="input_wrap">
            <label for="country">Nomor Dokumet</label>
            <input type="text" name="nomor_dokumen" >
          </div>
          <div class="input_wrap">
            <label for="country">Luas</label>
            <input type="text" name="luas" >
          </div>
          <div class="input_wrap">
            <label for="country">Status</label>
            <input type="text" name="status">
          </div>
          <div class="input_wrap">
            <label for="country">Asal Usul</label>
            <input type="text" name="asal_usul">
          </div>
          <div class="input_wrap">
            <label for="country">Harga</label>
            <input type="text" name="harga" >
          </div>
          <div class="input_wrap">
            <label for="country">Keterangan</label>
            <input type="text" name="keterangan" >
          </div>

          {{-- <div class="input_wrap">
            <label>Gender</label>
            <ul>
              <li>
                <label class="radio_wrap">
                  <input type="radio" name="gender" value="male" class="input_radio" checked>
                  <span>Male</span>
                </label>
              </li>
              <li>
                <label class="radio_wrap">
                  <input type="radio" name="gender" value="female" class="input_radio">
                  <span>Female</span>
                </label>
              </li>
            </ul>
          </div> --}}



           <div class="input_wrap">
            <input type="submit" value="Save" class="submit_btn">
          </div>
          <a class="btnn" href="{{url('administrator')}}">Kembali</a>
        </div>
      </form>
      {{-- @endforeach --}}
    </div>
  </div>
{{--
  <div class="container">
    <h2 class="data">Data Banggunan</h2> --}}
    {{-- @forelse ($banggunan as $b) --}}
	{{-- <table>
    <thead>
      <tr>
        <th>Kode Lokasi</th>
				<th>Kode Barang</th>
				<th>Kondisi Banggunan</th>
				<th>Letak Lokasi</th>
				<th>Luas</th>
				<th>Keterangan</th>
				<th>Option</th>
			</tr>
		</thead> --}}
		{{-- <tbody> --}}
      {{-- @foreach($banggunan as $bangguna) --}}
      {{-- @foreach($banggunan as $bang)
			<tr>
        <td>{{$bang->kode_lokasi}}</td>
				<td>{{$bang->kode_barang}}</td>
				<td>{{$bang->kondisi_banggunan}}</td>
				<td>{{$bang->letak_lokasi}}</td>
				<td>{{$bang->luas_lantai}}</td>
				<td>{{$bang->keterangan}}</td>
				<td><div class="dropdown"> 
          <button class="dropobtn">
            Option
          </button>
          <div class="dropdown-content">
            <a href="">Edit</a>
            <a href="">Hapus</a>
          </div>
          </div></td>
			</tr> --}}
				{{-- <td>{{$b->kode_barang}}</td>
				<td>{{$b->kondisi_banggunan}}</td>
				<td>{{$b->letak_lokasi}}</td>
				<td>{{$b->luas}}</td>
				<td>{{$b->keterangan}}</td>  --}}
			{{-- <tr>
				<td>Cell 1</td>
				<td>Cell 2</td>
				<td>Cell 3</td>
				<td>Cell 4</td>
				<td>Cell 5</td>
			</tr>
			<tr>
				<td>Cell 1</td>
				<td>Cell 2</td>
				<td>Cell 3</td>
				<td>Cell 4</td>
				<td>Cell 5</td>
			</tr>
			<tr>
				<td>Cell 1</td>
				<td>Cell 2</td>
				<td>Cell 3</td>
				<td>Cell 4</td>
				<td>Cell 5</td>
			</tr>
			<tr>
				<td>Cell 1</td>
				<td>Cell 2</td>
				<td>Cell 3</td>
				<td>Cell 4</td>
				<td>Cell 5</td>
			</tr> --}}
		{{-- </tbody> --}}
    {{-- @endforeach --}}
    {{-- @endforeach
	</table> --}}
  {{-- @empty
    @endforelse --}}
  {{-- </div>
</body>
</html> --}}
