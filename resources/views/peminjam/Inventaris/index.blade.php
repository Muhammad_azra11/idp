<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

{{-- <form action="" class="d-flex justify-content-center pt-5">
    <div class="form-group col-lg-5">
        <label for="">iiii</label>////
        <select class="form-control col-lg-5" name="" id="">
            <option value="">www</option>
            <option value="">www</option>
            <option value="">www</option>
        </select>
    </div>
</form>


<select class="form-select" aria-label="Default select example">
    <option selected>Open this select menu</option>
    <option value="1">One</option>
    <option value="2">Two</option>
    <option value="3">Three</option>
  </select> --}}



  <div class="col-12 c0l-md-8 offset-md-2 col-lg-6 offset-md-3">
    <h1 class="mb-5">Banguunan</h1>
    <button class="btn btn-primary"><a href="peminjam" style="color: white">Kembali</a></button>

    <div class="mt-5">
        @if(session('status'))
        <div class="alert {{session('alert-class')}}">
        {{session('status')}}
        </div>
        @endif
    </div>

{{-- {{$banggunan}} --}}
    <form action="banggunan" method="post">
        @csrf
        <div class="mb-3">
            <label for="id_banggunan" class="form-label">Banggunan </label>
                <select name="id_banggunan" id="id_banggunan" class="form-control">
                    <option value="">Select Banggunan</option>
                    @foreach ($banggunan as $item)
                        <option value="{{$item->id}}">{{$item->letak_lokasi}}</option>
                    @endforeach
                </select>
        </div>
        <div>
            <button class="btn btn-primary w-100" type="submit">Pinjam</button>
        </div>
    </form>
  </div>