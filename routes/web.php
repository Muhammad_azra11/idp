<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
// use App\Http\Controllers\BangunanController;
use App\Http\Controllers\InventarisController;
use App\Http\Controllers\InventarisHomeController;
use App\Http\Controllers\PeminjamController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BanggunanController;
use App\Http\Controllers\MekanikController;
use App\Http\Controllers\NonInventarisController;
use App\Http\Controllers\RedircetController;
// use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  jika user belum login
Route::group(['middleware' => 'guest'], function() {
    Route::get('/', [AuthController::class, 'login'])->name('login');
    Route::post('/', [AuthController::class, 'dologin']);

});

// untuk superadmin dan pegawai
Route::group(['middleware' => ['auth', 'checkrole:1,2,3']], function() {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/redirect', [RedircetController::class, 'cek']);
});


// untuk superadmin
Route::group(['middleware' => ['auth', 'checkrole:1']], function() {
    Route::get('/administrator', [AdminController::class, 'index']);
     Route::get('/tanah', [InventarisController::class, 'index'])->name('tanah.index');
     Route::post('/tanah', [InventarisController::class, 'store'])->name('tanah.store');
     Route::get('/tanahedit/{id}', [InventarisController::class, 'edit'])->name('tanah.edit');
     Route::put('/tanah/{id}', [InventarisController::class, 'update']);
     Route::get('non', [NonInventarisController::class, 'index'])->name('non.index');
     Route::post('non', [NonInventarisController::class, 'store'])->name('non.store');
     Route::get('nonedit/{id}', [NonInventarisController::class, 'edit'])->name('non.edit');
    //  Route::resource('/banggunan', BanggunanController::class)->only(['index', 'store']);
    //  Route::get('/banggunan/{id}/ubah/', [BanggunanController::class, 'edit']);
    //  Route::post('updatebnggunan/{id}', [BanggunanController::class, 'update']);
    //  Route::get('/banggunan', BanggunanController::class, 'index');
    //  Route::Resource('/banggunan', BanggunanController::class);
     Route::get('/banggunan', [BanggunanController::class, 'index'])->name('banggunan.index');
     Route::post('/banggunan', [BanggunanController::class, 'store'])->name('banggunan.store');
     Route::get('banggunanedit/{id}', [BanggunanController::class, 'edit'])->name('banggunan.edit');
     Route::put('banggunan/{id}', [BanggunanController::class, 'update']);
    //  Route::get('non', [BanggunanController::class, 'index']);
    //  Route::put('banggunan', [BanggunanController::class, 'edit'])->name('banggunan.edit');
});

// untuk pegawai
Route::group(['middleware' => ['auth', 'checkrole:2']], function() {
    Route::get('/peminjam', [PeminjamController::class, 'index']);
    Route::get('/banggunan', [PeminjamController::class, 'banggunan']);
    Route::post('/banggunan', [PeminjamController::class, 'store']);
});

Route::group(['middleware' => ['auth', 'checkrole:3']], function() {
    Route::get('/mekanik', [MekanikController::class, 'index']);
});
















// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', function () {
//     return view('auth.login');
// });

// Route::get('/pilihan', [InventarisHomeController::class, 'index']);

// Route::middleware([
//     'auth:sanctum',
//     config('jetstream.auth_session'),
//     'verified'
//     ])->group(function () {
//         Route::get('/dashboard', function () {
//         return view('dashboard');
//     })->name('dashboard');
// });
// Route::get('/redirects', [HomeController::class, 'index']);

// Route::get('/', function () {
//     return view('auth.login');
// });


// Route::middleware(['auth'])->group(function(){
    // Route::get('admin/home', [HomeController::class, 'adminHome']);
    // Route::get('/tanah', [InventarisController::class, 'index']);

    // Route::resource('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
    // Route::get('/peminjam', [App\Http\Controllers\HomeController::class, 'peminjam'])->name('home');
    // Route::get('/mekanik', [HomeController::class, 'mekanik']);
// });

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Auth::routes();

// Route::get('admin/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/loginn', [App\Http\Controllers\Auth\LoginController::class,  'login']);

// Route::get('peminjam', [HomeController::class, 'peminjam'])->name('home')->middleware();
// Auth::routes();

